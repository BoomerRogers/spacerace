using UnityEngine;
using System.Collections;

public class MainPlayer : MonoBehaviour {
	public GameObject 	laser_prefab;
	public GameObject	SpaceBackground;
	private	Vector3		targetPosition;
	public Transform	playerTransform;
	public float 		playerSpeed = 30f;
	public float 		fireRate = 0.1f;
	public float 		nextFire = 0.0f;
		void Start ( ){
//		playerTransform = transform;
//		targetPosition = playerTransform.position;
		//transform.position = new Vector3( 0, -12, 0) ;
	}
	void Update ( ){
		float spaceTravelled = playerSpeed * Time.deltaTime;
		transform.Translate( Vector3.right * Input.GetAxis( "Horizontal" ) * playerSpeed * Time.deltaTime );
		transform.Translate( Vector3.up * Input.GetAxis( "Vertical" ) * playerSpeed * Time.deltaTime );
		
		
//		Vector3 getPosition = transform.TransformPoint( Input.mousePosition );
//		transform.Rotate(0,0,( Mathf.Atan2( Input.mousePosition.y, Input.mousePosition.x ) ) );

//		

		
		
//		
//		playerTransform.position = Vector3.Lerp ( playerTransform.position, targetPosition, spaceTravelled );
//		
//		Mathf.Clamp( transform.position.y,-19,25 );
		if ( transform.position.x < -146 ){
			transform.position = new Vector3( -146, transform.position.y, transform.position.z );
		}
//		else if ( transform.position.x < -53 ){
//			transform.position = new Vector3( 53, transform.position.y, transform.position.z );
//		}
//		if ( transform.position.y > 25 ){
//			transform.position = new Vector3( transform.position.x, 25, transform.position.z );
//		}
//		else if ( transform.position.y < -23 ){
//			transform.position = new Vector3( transform.position.x, -23,transform.position.z );
//		}
		
		
		if ( Input.GetMouseButton( 0 ) && Time.time >= nextFire ) {
			nextFire = Time.time + fireRate;
			Vector3 LzrPosition = new Vector3( transform.position.x, transform.position.y +
											( transform.localScale.y / 2 ) );
			Instantiate( laser_prefab, LzrPosition, Quaternion.identity );
		}
	}
}
