﻿using UnityEngine;
using System.Collections;

public class SolarSystem : MonoBehaviour {
	
	public GameObject player;
	public float offSetX 			= 0;
	public float offSetY			= 0;
	public float offSetZ 			= 10;
	public float maximumDistance 	= 30;
	public float playerVelocity 	= 10;
	private float movementX;
	private float movementY;
	private float movementZ;
	void Update ( ) {
		movementX 					= ( ( player.transform.position.x + offSetX -
									this.transform.position.x) )/maximumDistance;
		movementZ 					= ( ( player.transform.position.z + offSetZ -
									this.transform.position.z) )/maximumDistance;
		movementY					= ( ( player.transform.position.y + offSetY -
									this.transform.position.y) )/maximumDistance;
		this.transform.position 	+= new Vector3( ( movementX * playerVelocity
									* Time.deltaTime ), ( movementY * playerVelocity
									* Time.deltaTime), ( movementZ * playerVelocity
									* Time.deltaTime ) ); 
	}
}
