using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
	public int laser_speed = 70;
	private Transform lzr_transform;
	
	// Use this for initialization
	void Start () {
		lzr_transform = transform;
	}
	// Update is called once per frame
	void Update () {
		//Make the laser shoot out and go up
		transform.Translate(Vector3.up * laser_speed * Time.deltaTime);
		if (lzr_transform.position.y > 50){
			Destroy(gameObject);
		}
	}
}